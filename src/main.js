import Vue from "vue";
import App from "./App.vue";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import router from "./router";
import Vuex from "vuex";

Vue.use(Vuex);

Vue.use(ElementUI);
const store = new Vuex.Store({
  state: {
    authorization: false,
    userId: "",
  },
});

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
  store: store,
}).$mount("#app");
